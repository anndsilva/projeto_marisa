#language: pt 

Funcionalidade: Validação do formulário de login do site Marisa para acessar o chat

  Contexto: Testar Funcionalidades do formulário de login do site Marisa
    Dado que visito o site Marisa

  Cenario: Realizar o login no chat, utilizando a área de atendimento como "Cartão/Loja Fisica"
    Quando preencho o campo nome
    E preencho o campo email 
    E preencho o campo CPF
    E preencho o campo telefone
    E seleciono a opção "Cartão/Loja Fisica"
    E clico no botao "Entrar no Chat"
    Entao verifico se login foi efetuado com sucesso

  Cenario: Realizar o login no chat, utilizando a área de atendimento como "Loja Virtual"
    Quando preencho o campo nome
    E preencho o campo email 
    E preencho o campo CPF
    E preencho o campo telefone
    E seleciono a opção "Loja Virtual"
    E clico no botao "Entrar no Chat"
    Entao verifico se login foi efetuado com sucesso

  Cenario: Realizar o login no chat, sem informar a área de atendimento
    Quando preencho o campo nome
    E preencho o campo email 
    E preencho o campo CPF
    E preencho o campo telefone
    E clico no botao "Entrar no Chat"
    Entao verifico se login foi efetuado com sucesso

    