#language: pt

Funcionalidade: Validação do formulário de login do site Marisa para acessar o chat

  Contexto: Testar Funcionalidades do formulário de login do site Marisa
    Dado que visito o site Marisa

  Cenario: Logar no chat com CPF invalido 
    Quando preencho o campo email 
    E preencho o campo CPF com um numero invalido
    E preencho o campo nome
    E preencho o campo telefone
    E clico no botao "Entrar no Chat"
    Entao verifico se foi gerado a mensagem "CPF inválido"

  Cenario: Verificar se a máscara para o CPF está sendo aplicada.  
    Quando preencho o campo CPF
    E preencho o campo email
    E preencho o campo nome
    E preencho o campo telefone
    E clico no botao "Entrar no Chat"
    Entao verifico se a máscara de CPF está sendo aplicada


