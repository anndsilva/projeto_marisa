 #language: pt

Funcionalidade: Validação do formulário de login do site Marisa para acessar o chat

  Contexto: Testar Funcionalidades do formulário de login do site Marisa
    Dado que visito o site Marisa
 
  Cenario: Logar no chat sem preencher os dados para verificar à obrigatoridade
    Quando tento logar sem preencher os campos obrigatorios
    Entao verifico se foi gerado a mensagem de erro

  Cenario: Logar no chat sem preencher o email para verificar à obrigatoridade
    Quando preencho o campo nome
    E preencho o campo CPF
    E preencho o campo telefone
    E clico no botao "Entrar no Chat"
    Entao verifico se encontro o erro 

  Cenario: Logar no chat sem preencher o CPF para verificar à obrigatoridade
    Quando preencho o campo nome
    E preencho o campo email
    E preencho o campo telefone
    E clico no botao "Entrar no Chat"
    Entao verifico se encontro o erro

  Cenario: Logar no chat sem preencher o nome para verificar à obrigatoridade
    Quando preencho o campo CPF
    E preencho o campo email
    E preencho o campo telefone
    E clico no botao "Entrar no Chat"
    Entao verifico se encontro o erro 